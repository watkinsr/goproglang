package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	start := time.Now()
	for i := 0; i < len(os.Args); i++ {
		fmt.Printf("%v %v\n", i, os.Args[i])
	}
	fmt.Printf("%dns elapsed\n", time.Since(start).Nanoseconds())
}
