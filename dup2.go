// Dup2 prints the count and text of lines that appear more than once
//in the input. It reads from stdin or from a list of named files.
package main

import (
	"bufio"
	"fmt"
	"os"
)

type val struct {
	amount int
	files  []string
}

func main() {
	counts := make(map[string]val)
	files := os.Args[1:]
	if len(files) == 0 {
		countLines(os.Stdin, counts)
	} else {
		for _, arg := range files {
			f, err := os.Open(arg)
			if err != nil {
				fmt.Fprintf(os.Stderr, "dup2: %v\n", err)
			}
			countLines(f, counts)
			f.Close()
		}
	}
	for line, v := range counts {
		if v.amount > 1 {
			fs := unique(v.files)
			fmt.Printf("%d\t%s\t%s\n", v.amount, line, fs)
		}
	}
}

func unique(strSlice []string) []string {
	keys := make(map[string]bool)
	list := []string{}
	for _, entry := range strSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func countLines(f *os.File, counts map[string]val) {
	input := bufio.NewScanner(f)
	for input.Scan() {
		t := input.Text()
		pair := counts[t]
		pair.amount++
		pair.files = append(pair.files, f.Name())
		// fmt.Printf("%s %d %s\n", input.Text(), pair.amount, pair.files)
		counts[t] = pair
	}
}
